from django.http import HttpResponseRedirect
from django.shortcuts import render

from sitio.models import Noticia
from sitio.forms import FormContacto

def contacto(request):
	
	if request.method == 'POST':
		formulario = FormContacto(request.POST)
		
		if formulario.is_valid():
			print('Alguien nos quiere decir:')
			print(formulario.cleaned_data['titulo'])
			print(formulario.cleaned_data['texto'])
	
			return HttpResponseRedirect('/inicio/')

	else:	
		formulario = FormContacto()
	
	return render(request, 'contacto.html', {
			'form':formulario,
		})

def inicio(request):
	noticias = Noticia.objects.all().select_related('categoria')
	return render(request, 'inicio.html', {
		'lista_noticias': noticias,
	})