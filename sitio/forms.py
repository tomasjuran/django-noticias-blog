from django import forms

class FormContacto(forms.Form):
	titulo = forms.CharField(max_length=50)
	texto = forms.CharField(max_length=255)
	mail = forms.EmailField(required=False)

	def clean_mail(self):
		mail = self.cleaned_data['mail']

		if not mail.endswith('.com'):
			raise forms.ValidationError('Solo .com por favor')

		return mail
